angular.module('appServices', ['config'])
	.factory('appServices', function ($http, $rootScope, ENV) {
		var api = ENV.apiEndpoint;
		return {
			profile : function () {
				return $http.get(api + '/api/user/profile')
			},
			generalReports : function () {
				return $http.get(api + '/api/reporte/general/lite/'+$rootScope.rinit+'/'+$rootScope.rlimit)
			},
			infoReport : function (id) {
				return $http.get(api + '/api/reporte/general/id/' + id)
			},
			generalUserReports : function (init, limit) {
				return $http.get(api + '/api/reporte/user/'+init+'/'+limit)
			}
		}
	})