# TráficoChilango App
App móvil desarrollada con Ionic. 

TráficoChilango App tiene la finalidad de proveer a los usuarios del Sistema de Transporte Colectivo Metro (STCM) de la Cd. de México una herramienta para crear y ver reportes de incidentes en dicho sistema ayudandoles así a establecer rutas alternas en su viaje.

### Version
0.1.1 (Dev)

### Tech

* [Ionic Framework]
* [Ionic Material]
* [AngularJS]