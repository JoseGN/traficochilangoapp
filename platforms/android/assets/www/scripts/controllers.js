'use strict';
angular.module('mainControllers', [])
	.controller('AppCtrl', function ($scope, $auth, $state, $ionicPopover, $timeout) {
	    // Form data for the login modal
	    $scope.isExpanded = false;
	    $scope.hasHeaderFabLeft = false;
	    $scope.hasHeaderFabRight = false;

	    var navIcons = document.getElementsByClassName('ion-navicon');
	    for (var i = 0; i < navIcons.length; i++) {
	        navIcons.addEventListener('click', function () {
	            this.classList.toggle('active');
	        });
	    }

	    ////////////////////////////////////////
	    // Logout App
	    ////////////////////////////////////////
	    $scope.logout = function () {
		    $auth.logout()
		    	.then(function () {
		    		$state.go('login');
		    	})
		};

	    ////////////////////////////////////////
	    // Layout Methods
	    ////////////////////////////////////////

	    $scope.hideNavBar = function () {
	        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
	    };

	    $scope.showNavBar = function () {
	        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
	    };

	    $scope.noHeader = function () {
	        var content = document.getElementsByTagName('ion-content');
	        for (var i = 0; i < content.length; i++) {
	            if (content[i].classList.contains('has-header')) {
	                content[i].classList.toggle('has-header');
	            }
	        }
	    };

	    $scope.setExpanded = function (bool) {
	        $scope.isExpanded = bool;
	    };

	    $scope.setHeaderFab = function (location) {
	        var hasHeaderFabLeft = false;
	        var hasHeaderFabRight = false;

	        switch (location) {
	            case 'left':
	                hasHeaderFabLeft = true;
	                break;
	            case 'right':
	                hasHeaderFabRight = true;
	                break;
	        }

	        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
	        $scope.hasHeaderFabRight = hasHeaderFabRight;
	    };

	    $scope.hasHeader = function () {
	        var content = document.getElementsByTagName('ion-content');
	        for (var i = 0; i < content.length; i++) {
	            if (!content[i].classList.contains('has-header')) {
	                content[i].classList.toggle('has-header');
	            }
	        }

	    };

	    $scope.hideHeader = function () {
	        $scope.hideNavBar();
	        $scope.noHeader();
	    };

	    $scope.showHeader = function () {
	        $scope.showNavBar();
	        $scope.hasHeader();
	    };

	    $scope.clearFabs = function () {
	        var fabs = document.getElementsByClassName('button-fab');
	        if (fabs.length && fabs.length > 1) {
	            fabs[0].remove();
	        }
	    };
	})
	.controller('loginCtrl', function ($scope, $http, $ionicPlatform, ionicMaterialInk, ionicMaterialMotion, $ionicSideMenuDelegate, $ionicLoading, $cordovaSplashscreen, $cordovaStatusbar, $cordovaDevice, $cordovaOauth, $cordovaVibration, $cordovaCamera, $auth, $state, $timeout) {
		$timeout(function (){
			ionicMaterialInk.displayEffect();
			ionicMaterialMotion.ripple();
			// Oculta barra de header
	        $scope.$parent.hideHeader();
		},0);

		$scope.appName = 'TráficoChilango';
		$scope.photo=null;

		$scope.facebookLogin = function () {
		$cordovaOauth.facebook('909951935714936', ['email', 'public_profile', 'publish_actions'])
			.then(function (result) {
				alert("Response Object -> " + JSON.stringify(result))

				// Debug
				$http.get('http://172.19.0.150:4000/auth/facebook/token/'+result.access_token)
				//$http.post('http://172.19.0.150:4000/auth/facebook/token', {access_token:result.access_token})
				.success(function (res){
					alert('response: '+JSON.stringify(res))
				})
				.error(function (err){
					alert('error: '+JSON.stringify(err));
				})

			}, function (error) {
				alert(error)
			});
		}
		$scope.twitterLogin = function () {
		$cordovaOauth.twitter('UMbD4Jgvvp74iVnNlA4zJys8g', 'R2rouOEepfdncO6PuGqZglnUkfYhGlahnBHjWJv6mfVlXX0a5b', {redirect_uri:'http://127.0.0.1/callback'})
			.then(function (result) {
				alert("Response Object -> " + JSON.stringify(result))

				// Debug
				$http.post('http://172.19.0.150:4000/api/consuelas', {twitter_auth:result})
				.success(function (res){
					alert('response: '+res)
				})
				.error(function (err){
					alert('error: '+err);
				})

			}, function (error) {
				alert(error)
			});
		}

		$scope.authenticate = function (provider) {
			$auth.authenticate(provider)
				.then(function (result){
					console.log(result)
					$state.go('app.reports');
				})
				.catch(function (err){
					console.log(err);
			});
		};



		$scope.vibration = function () {
			$cordovaVibration.vibrate(100);
		}

		$scope.shot = function () {
			// camera
			var options = {
		      quality: 50,
		      destinationType: Camera.DestinationType.DATA_URL,
		      sourceType: Camera.PictureSourceType.CAMERA,
		      allowEdit: true,
		      encodingType: Camera.EncodingType.JPEG,
		      targetWidth: 100,
		      targetHeight: 100,
		      popoverOptions: CameraPopoverOptions,
		      saveToPhotoAlbum: false
		    };

		    $cordovaCamera.getPicture(options).then(function (imageData) {
					$scope.photo = "data:image/jpeg;base64," + imageData;
				}, function (err) {
					alert(error)
			});
		}
		$ionicPlatform.ready(function () {
			$scope.device = $cordovaDevice.getVersion();
			$cordovaStatusbar.overlaysWebView(true);
			$cordovaStatusbar.styleHex('#DD7212');


		});

	 })
	.controller('reportsCtrl', function ($scope, $rootScope, $http, $auth, $state, $ionicModal, $timeout, $ionicLoading, ionicMaterialInk, ionicMaterialMotion, $ionicActionSheet, appServices) {

		if (!$auth.isAuthenticated()) {
			$state.go('login');
			return;
		}

		$ionicLoading.show();

		/*appServices.profile()
			.success(function (res) {
				$rootScope.user = res.usuario;
			})
			.error(function (err) {
				console.log(err);
			})*/

		$scope.reports = [];
		$scope.moreReportsCanBeLoaded = true;


		$scope.loadMore = function() {
			appServices.generalReports()
				.success(function (res) {
					if(res.reportes.length == 0) $scope.moreReportsCanBeLoaded = false;
					$scope.reports = $scope.reports.concat(res.reportes);
					console.log($rootScope);
					$rootScope.incrementLimit(10);
					$scope.$broadcast('scroll.infiniteScrollComplete');
				});
		};
		appServices.generalReports()
			.success(function (res) {
				$ionicLoading.hide();
				$scope.reports = res.reportes;
				$rootScope.incrementLimit(10);

				$timeout(function () {
					// Set Motion
					ionicMaterialMotion.blinds();
					// Set Ink
					ionicMaterialInk.displayEffect();
				}, 200);
			})
			.error(function (err) {
				console.log(err);
			});


		$scope.Hell = 'F***ck';


		// El modal para ver fotos en grande
		$ionicModal.fromTemplateUrl('my-modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
			})
			.then(function (modal) {
				$scope.modal = modal;
			});

			$scope.openModal = function () {
					$scope.modal.show();
			};
			$scope.closeModal = function () {
				$scope.modal.hide();
			};

		 // Triggered on a button click, or some other target
		$scope.actionSheet = function () {

			// Show the action sheet
			var hideSheet = $ionicActionSheet.show({
				buttons: [
					{ text: '<i class="icon ion-social-facebook"></i>Facebook' },
					{ text: '<i class="icon ion-social-twitter"></i>Twitter' },
					{ text: '<i class="icon ion-social-googleplus"></i>GooglePlus' }
				],
				//destructiveText: 'Delete',
				titleText: 'Compartir reporte',
				cancelText: 'Cancelar',
				cancel: function () {
					// add cancel code..
				},
				buttonClicked: function (index) {
					return true;
				}
			});

			// For example's sake, hide the sheet after two seconds
			/*$timeout(function () {
				hideSheet();
			}, 2000);*/

		};

	})
	.controller('settingsCtrl', function ($scope, $rootScope, $stateParams, $timeout, ionicMaterialMotion, $ionicLoading, ionicMaterialInk, appServices) {
		// Set Header
		$scope.$parent.showHeader();
		$scope.$parent.clearFabs();
		$scope.isExpanded = true;
		$scope.$parent.setExpanded(false);
		$scope.$parent.setHeaderFab(false);
		$ionicLoading.show();
		appServices.profile()
			.success(function (res) {
				$ionicLoading.hide();
				$scope.user = res.usuario;
				// Set Motion
				$timeout(function() {
					ionicMaterialMotion.slideUp({
						selector: '.slide-up'
					});
				}, 300);

				$timeout(function() {
					ionicMaterialMotion.fadeSlideInRight({
						startVelocity: 3000
					});
				}, 700);

			})
			.error(function (err) {
				console.log(err);
			})



		// Set Ink
		ionicMaterialInk.displayEffect();
	});