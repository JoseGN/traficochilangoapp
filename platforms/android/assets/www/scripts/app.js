// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
'use strict';
angular.module('starter', ['ionic', 'mainControllers', 'ionic-material', 'ionMdInput', 'ngCordova', 'ngCordovaOauth', 'satellizer', 'config', 'appServices'])

	.run(function ($ionicPlatform, $cordovaPush, $http, $rootScope, ENV) {

		$rootScope.rinit = 0;
		$rootScope.rlimit = 10;
		$rootScope.user = {};

		$rootScope.incrementLimit = function (n) {
			$rootScope.rinit = $rootScope.rinit + n;
			$rootScope.rlimit = $rootScope.rlimit + n;
		}

		$ionicPlatform.ready(function () {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			if(window.cordova && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			}
			if(window.StatusBar) {
				StatusBar.styleDefault();
			}
		});




		// Registra el dispositivo en GCM al cargar la app
		document.addEventListener('deviceready', function() {
			var androidConfig = {
				'senderID': '000000000000'
			};

			$cordovaPush.register(androidConfig)
				.then(function (result) { //Suceess
					//alert('result: '+result);
					console.log('result: '+result);
					/*$http.post('http://172.19.0.150:4000/api/consuelas', {resultado:result})
						.success(function (res){
							alert('response: '+res)
						})
						.error(function (err){
							alert('error: '+err);
						})*/
				}, function (err) { // Error
					alert('error: '+err);
				});

			$rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
				switch (notification.event) {
					case 'registered':
						if (notification.regid.length > 0) {
							alert('registration ID = ' + notification.regid);
						}
						break;

					case 'message':
						// this is the actual push notification. its format depends on the data model from the push server
						alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
						break;

					case 'error':
						alert('GCM error = ' + notification.msg);
						break;

					default:
						alert('An unknown GCM event has occurred');
						break;
				}
			});



			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
				// Success!
			// }, function(err) {
				// Error
			// })

		}, false);

	})

	.config(function ($authProvider, ENV) {

	// OAuth popup should expand to full screen with no location bar/toolbar.
	    var commonConfig = {
			popupOptions: {
				location: 'no',
				toolbar: 'no',
				width: window.screen.width,
				height: window.screen.height
			},
	    	redirectUri : 'http://localhost:8100/'
	    };

	    $authProvider.baseUrl = ENV.apiEndpoint;
	    $authProvider.tokenPrefix = 'TC';
	    $authProvider. authToken = 'TCBearer';

	    if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
			$authProvider.platform = 'mobile';
			commonConfig.redirectUri = 'http://localhost/';
	    } else {
			$authProvider.withCredentials = false;
		}

	    $authProvider.facebook(angular.extend({}, commonConfig, {
			clientId: '909951935714936',
			//responseType: 'token',
			url: '/auth/self/facebook',
			scope: 'email, public_profile, publish_actions'
	    }));

	    $authProvider.twitter(angular.extend({}, commonConfig, {
			url: '/auth/self/twitter'
	    }));

	    $authProvider.google(angular.extend({}, commonConfig, {
			clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com',
			url: 'http://localhost:3000/auth/google'
	    }));

	})

	.config(function ($stateProvider, $urlRouterProvider) {

		// stateProvider config //Rutas
		$stateProvider
			.state('app', {
				url: '/app',
				abstract: true,
				templateUrl: 'templates/menu.html',
				controller: 'AppCtrl'
			})
			.state('app.reports', {
		        url: '/reports',
		        views: {
		            'menuContent': {
		                templateUrl: 'templates/reports.html',
		                controller: 'reportsCtrl'
		            },
		            'fabContent': {
	                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900 spin"><i class="icon ion-plus"></i></button>',
	                controller: function ($timeout) {
	                    $timeout(function () {
	                        document.getElementById('fab-profile').classList.toggle('on');
	                    }, 800);
	                }
	            }
		        }
		    })
		    .state('app.settings', {
		        url: '/settings',
		        views: {
		            'menuContent': {
		                templateUrl: 'templates/settings.html',
		                controller: 'settingsCtrl'
		            },
		            'fabContent': {
	                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900 spin"><i class="icon ion-plus"></i></button>',
	                controller: function ($timeout) {
	                    $timeout(function () {
	                        document.getElementById('fab-profile').classList.toggle('on');
	                    }, 800);
	                }
	            }
		        }
		    })
			.state('login', {
				url: '/login',
				templateUrl: 'templates/login.html',
				controller: 'loginCtrl'
			})
			/*.state('app.login', {
		        url: '/login',
		        views: {
		            'menuContent': {
		                templateUrl: 'templates/login.html',
		                controller: 'mainCtrl'
		            },
		            'fabContent': {
		                template: ''
		            }
		        }
		    })*/;

		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/reports');

	});
